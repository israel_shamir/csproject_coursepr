﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GpsMonitoring
{
    public class GpsPoints
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
        public DateTime timeStamp { get; set; }
        public double speed { get; set; }

        public void ParseString(string line)
        {

            string[] str = line.Split('@');

            latitude = double.Parse(str[0], CultureInfo.InvariantCulture);
            longitude = double.Parse(str[1], CultureInfo.InvariantCulture);
            timeStamp = DateTime.Parse(str[2], CultureInfo.InvariantCulture);
            speed =  double.Parse(str[3], CultureInfo.InvariantCulture);

        }

         public static List<GpsPoints> ReadFile(string filename)
            {
                List<GpsPoints> newList = new List<GpsPoints>();
                using (StreamReader sr = new StreamReader(filename))
                {
                    string line;
                    while ((line = sr.ReadLine()) != "")
                    {
                        GpsPoints newP = new GpsPoints();
                        newP.ParseString(line);
                        newList.Add(newP);
                    }
                }
 
                return newList;
            }

         public List<List<GpsPoints>> GetDir() 
         {
             string[] dirs = Directory.GetFiles(@"C:\Users\O Propio Regil\Documents\C#Project\GitPro\csproject_coursepr\GpsMonitoring\Tracks", "*.csv");

             List<List<GpsPoints>> newList = new List<List<GpsPoints>>();

             foreach (string d in dirs)
             {
                newList.Add(ReadFile(d));
             }

             return newList;
         }

    }
}
