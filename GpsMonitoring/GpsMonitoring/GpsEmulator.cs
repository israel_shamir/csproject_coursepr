﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GpsMonitoring;
using System.Threading;
using System.Diagnostics;

namespace GpsEmulatorLib
{
    public class GpsEmulator
    {
        List<List<GpsPoints>> track;
        List<Car> newCar;
        Timer newTimer;
        public GpsEmulator()
        {


            newCar = GetCarsAndGpsPoints();
            GpsPoints newObj = new GpsPoints();
            track = newObj.GetDir();
            newTimer = new Timer(timerhandlerob, null, 0, 5000);


        }
        
        public void timerhandlerob(object state)
        {
            for (int i = 0; i < newCar.Count; i++)
            {
                var pointnum = newCar[i].PointNumber + 1;
                var tracknum = newCar[i].TrackNumber;

                if (pointnum > track[tracknum].Count - 1)
                    pointnum = 0;

                var point = track[tracknum][pointnum];

                 //тут сохранение в базу
                using(GPSTrackInfoEntities context = new GPSTrackInfoEntities())
                {
                   
                    CarTrack gpspoints = new CarTrack();

                    gpspoints.Latitude = point.latitude;
                    gpspoints.Longitude = point.longitude;
                    gpspoints.TimeStamp = DateTime.Now;
                    gpspoints.Speed = point.speed;
                    gpspoints.IdCar = newCar[i].IdCar;


                    context.CarTrack.Add(gpspoints);

                    foreach (var e in context.ChangeTracker.Entries())
                    {
                        Debug.WriteLine(string.Format("{0} - {1}", e, e.State));
                    }

                    context.SaveChanges();

                    foreach (var e in context.ChangeTracker.Entries())
                    {
                        Debug.WriteLine(string.Format("{0} - {1}", e, e.State));
                    }
                }
                newCar[i].PointNumber = pointnum;
            }
        }

        public static List<Car> GetCarsAndGpsPoints()
        {
            Random n = new Random();
            GpsPoints newObj = new GpsPoints();
            List<List<GpsPoints>> newList = newObj.GetDir();

            var context = new GPSTrackInfoEntities();
            var cars = context.Car.ToList();

            foreach (var c in cars)
            {
                c.TrackNumber = n.Next(newList.Count);
            }

            return cars;
        }










    }
}
