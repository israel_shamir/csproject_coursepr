﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Threading.Tasks;

namespace GpsMonitoring
{
   public class GpsPoint
    {
        public double latitude {get; set;}
        public double longitude {get; set;}
        public DateTime timeStamp {get; set;}
        public double speed {get; set;}
    }
}
