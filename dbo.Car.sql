﻿CREATE TABLE [dbo].[Car] (
    [IdCar]     INT identity(1, 1),
    [Model]     NVARCHAR (15) NOT NULL,
    [RegNumber] NVARCHAR (10) NOT NULL,
    PRIMARY KEY CLUSTERED ([IdCar] ASC)
);

