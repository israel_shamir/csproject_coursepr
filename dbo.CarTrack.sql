﻿CREATE TABLE [dbo].[CarTrack] (
    [IdCar]     INT not null,
    [TimeStamp] DATETIMEOFFSET (7) NOT NULL,
    [Longitude] FLOAT (53)         NOT NULL,
    [Latitude]  FLOAT (53)         NOT NULL,
    [Speed]     FLOAT (53)         NOT NULL,
	CONSTRAINT PK_CarTrack PRIMARY KEY(IdCar, [TimeStamp] desc),
    CONSTRAINT [FK_CarTrack_Car] FOREIGN KEY ([IdCar]) REFERENCES [dbo].[Car] ([IdCar])
);
